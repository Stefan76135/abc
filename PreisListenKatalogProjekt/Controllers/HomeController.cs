﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;

namespace PreisListenKatalogProjekt.Controllers
{
    public class HomeController : Controller
    {
        //Möglichkeit 4
        //Get Home
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        ////Die Index Funktion ist für Möglichkeit 3
        //public ActionResult Index(int page=1, string sort = "FahrzeugHersteller", string sortdir="asc", string search="")
        //{
        //    int pageSize = 10;
        //    int totalRecord = 0;
            
        //    if (page < 1) page = 1;

        //    int skip = (page * pageSize) - pageSize;
        //    var data = GetPreistabelle(search, sort, sortdir, skip, pageSize, out totalRecord);

        //    ViewBag.TotalRows = totalRecord;
        //    ViewBag.search = search;

        //    return View(data);
        //}

        //Die Index Funktion ist für Möglickeit 1 und Möglichkeit 2
        ////Get Home
        //public ActionResult Index()
        //{
        //    //ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

        //    return View();            
        //}

        //write Action for return json Data
    [HttpPost]
        public ActionResult LoadData()
        {

        //Möglichkeit 2 Start
        //using (PreislisteEntities dc = new PreislisteEntities())
        //{
        //    var data = dc.Preistabelle.OrderBy(a => a.FahrzeugHersteller).ToList();
        //    return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        //}
        //Möglichkeit 2 Ende

        //Möglichkeit 1 Start
            //Get Parameters

            //get Start(paging start index) and length (page size for paging)
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //get Sort columns value
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            int pagesize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            int totalRecords = 0;

            using (PreislisteEntities dc = new PreislisteEntities())
            {
                var v = (from a in dc.Preistabelle select a);

                //Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    v = v.OrderBy(sortColumn + " " + sortColumnDir);
                }

                totalRecords = v.Count();
                var data = v.Skip(skip).Take(pagesize).ToList();
                return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data }, JsonRequestBehavior.AllowGet);
            }
        //Möglichkeit 1 Ende
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        // Möglickeit 3 Start
        public List<Preistabelle> GetPreistabelle(string search, string sort, string sortdir, int skip, int pageSize, out int totalRecord)
        {
            using (PreislisteEntities dc = new PreislisteEntities())
            {
                var v = (from a in dc.Preistabelle
                         where
                            a.FahrzeugHersteller.Contains(search) ||
                            a.FahrzeugTyp.Contains(search) ||
                            a.Gangzahl.Contains(search) ||
                            a.GetriebeCode.Contains(search) ||
                            a.Baujahr_Von_Bis.Contains(search)
                         select a);
                totalRecord = v.Count();

                List<Preistabelle> x = v.ToList();

                v = v.OrderBy(sort + " " + sortdir);

                List<Preistabelle> y = v.ToList();

                if(pageSize > 0)
                {
                    v = v.Skip(skip).Take(pageSize);
                }
                List<Preistabelle> z = v.ToList();
                return v.ToList();
            }
        }
        // Möglickeit 3 Ende
    }
}
